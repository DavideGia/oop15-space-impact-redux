package spaceimpact.utilities;

/**
 * Enumeration to define possible user input
 */
public enum Input {
    W, A, S, D, SPACE;
}
