package spaceimpact.controller;

import spaceimpact.view.View;
import spaceimpact.view.ViewImpl;

/**
 * Class used to start the application.
 */
public final class Application {

    /**
     * Start a new application.
     */
    public static void main(final String[] args) {
        final Controller c = new ControllerImpl();
        final View v = new ViewImpl(c);
        c.setView(v);
        v.startView();
    }

    private Application() {
    }
}