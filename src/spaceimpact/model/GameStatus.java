package spaceimpact.model;

/**
 * Enumeration to define the status of the model.
 */
public enum GameStatus {
    /**
     * Player win if maximum enemy spawns is reached and there are no more living enemies.
     */
	Won,
	/**
     * Player lose if his life reach 0 or below.
     */
	Over,
	/**
	 * The game is running.
	 */
	Running;
}
