package spaceimpact.model.spawners;

import java.util.List;

import spaceimpact.model.Location;
import spaceimpact.model.entities.Projectile;

/** 
 * Weapon Interface<br>
 * A weapon is effectively a projectiles factory.<br>
 * Provides method to enhance near all weapon parameters.
 */
public interface Weapon {

	/** Shoot a Projectile<br>
	 * Create and shoot a new projectile.
	 * @param loc Location from which shoot
	 * @return A new projective object
	 */
	List<Projectile> shoot(final Location loc);
	
	/**
	 * Control if the weapon is ready to shoot or need to cool down.
	 * @return boolean True if the weapon is ready to shoot, False if needs to cool down
	 */
	boolean isReadyToShoot();
	
	/**
	 * Cool down the weapon.
	 */
	void coolDown();
	
	/**
	 * Getter for weapon damage.
	 * @return int As the current damage value
	 */
	int getDamage();
	
	/**
	 * Increase the number of projectiles that the weapon can shoot.
	 */
	void increaseProjectiles();
	
	/**
	 * Increase power (projectiles damage) of the weapon.
	 * @param increment As the damage increment (Integer)
	 * @throws IllegalArgumentException if the increment value is negative
	 */
	void increaseDamage(final int increment) throws IllegalArgumentException;
	
	/**
	 * Decrease cool down time of the weapon.
	 * @param decrement As the decrement (in ticks) in the cool down countdown
	 * @throws IllegalArgumentException if the decrement value is negative
	 */
	void decreaseCoolDown(final int decrement) throws IllegalArgumentException;
	
	/**
	 * Getter for the current number of projectiles that the weapon can shoot.
	 * @return projectiles Number of projectiles that the weapon can shoot
	 */
	int getProjectilesCount();
	
}
