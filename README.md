## Space Impact Redux ##

A clone of the classic game "Space Impact" developed in Java using JavaFX. The game was developed using the MVC pattern. We were given the best score by the professor and (as of his suggestion) we are going to publish it on Steam Greenlight some time early next year.
Multiple design patterns were used during the development of the game and we focused on making the code as extensible as possible.